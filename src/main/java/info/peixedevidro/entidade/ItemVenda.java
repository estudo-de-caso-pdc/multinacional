package info.peixedevidro.entidade;

import info.peixedevidro.negocio.comum.Identificavel;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.persistence.*;
import javax.validation.constraints.DecimalMin;
import java.io.Serializable;

@Entity
@IdClass(ItemVendaPK.class)
public class ItemVenda implements Identificavel<ItemVendaPK> {
    private static final long serialVersionUID = -8903045233875189828L;

    @Id
    @ManyToOne
    private Item item;

    @Id
    @ManyToOne
    private Venda venda;

    @DecimalMin(value = "0")
    private Integer quantidade;

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public Venda getVenda() {
        return venda;
    }

    public void setVenda(Venda venda) {
        this.venda = venda;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ItemVenda itemVenda = (ItemVenda) o;

        return new EqualsBuilder()
                .append(item, itemVenda.item)
                .append(venda, itemVenda.venda)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(item)
                .append(venda)
                .toHashCode();
    }

    @Override
    public ItemVendaPK getId() {
        return new ItemVendaPK(item.getId(), venda.getId());
    }
}
