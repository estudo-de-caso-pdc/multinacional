package info.peixedevidro.entidade;

import info.peixedevidro.negocio.dto.ItemDto;
import info.peixedevidro.negocio.dto.ItemVendaDto;
import info.peixedevidro.negocio.dto.SedeDto;
import info.peixedevidro.negocio.dto.VendaDto;
import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface VendaMapper {
    VendaMapper MAPPER = Mappers.getMapper(VendaMapper.class);

    @IterableMapping(elementTargetType = VendaDto.class)
    List<VendaDto> toListVendaDto(List<Venda> entities);

    VendaDto toVendaDto(Venda entity);

    @IterableMapping(elementTargetType = ItemVendaDto.class)
    List<ItemVendaDto> toListItemVendaDto(List<ItemVenda> entities);

    @Mapping(target = "venda", ignore = true)
    ItemVendaDto toItemVendaDto(ItemVenda entity);

    ItemDto toItemDto(Item entity);

    SedeDto toSedeDto(Sede entity);

    Venda toVenda(VendaDto dto);

    Venda toSede(SedeDto dto);

    Sede toSete(SedeDto dto);

    @Mapping(target = "venda", ignore = true)
    ItemVenda toItemVenda(ItemVendaDto dto);

    Item toItem(ItemDto dto);

    void updateVenda(VendaDto dto, @MappingTarget Venda entity);
}
