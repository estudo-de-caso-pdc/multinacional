package info.peixedevidro.entidade;

import info.peixedevidro.negocio.dto.ItemDto;
import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface ItemMapper {
    ItemMapper MAPPER = Mappers.getMapper(ItemMapper.class);

    @IterableMapping(elementTargetType = ItemDto.class)
    List<ItemDto> toListItemDto(List<Item> entities);

    ItemDto toItemDto(Item entity);

    Item toItem(ItemDto itemDto);

    void updateItem(ItemDto itemDto, @MappingTarget Item entity);
}
