package info.peixedevidro.entidade;

import info.peixedevidro.negocio.dto.ItemDto;
import info.peixedevidro.negocio.dto.ItemVendaDto;
import info.peixedevidro.negocio.dto.SedeDto;
import info.peixedevidro.negocio.dto.VendaDto;
import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface ItemVendaMapper {
    ItemVendaMapper MAPPER = Mappers.getMapper(ItemVendaMapper.class);

    @IterableMapping(elementTargetType = ItemVendaDto.class)
    List<ItemVendaDto> toListItemVendaDto(List<ItemVenda> entities);

    ItemVendaDto toItemVendaDto(ItemVenda entity);

    ItemDto toItemDto(Item entity);

    VendaDto toVendaDto(Venda entity);

    SedeDto toSedeDto(Sede entity);

    @IterableMapping(elementTargetType = ItemVenda.class)
    List<ItemVenda> toListItemVenda(List<ItemVendaDto> dtoList);

    ItemVenda toItemVenda(ItemVendaDto dto);

    Item toItem(ItemDto dto);

    Venda toVenda(VendaDto dto);

    Sede toSede(SedeDto dto);

    void updateItemVenda(ItemVendaDto dto, @MappingTarget ItemVenda entity);
}
