package info.peixedevidro.entidade;

import java.io.Serializable;

public class ItemVendaPK implements Serializable{
    private static final long serialVersionUID = -9113680713030944751L;

    private Long item;
    private Long venda;

    public ItemVendaPK() {
    }

    public ItemVendaPK(Long item, Long venda) {
        this.item = item;
        this.venda = venda;
    }

    public Long getItem() {
        return item;
    }

    public void setItem(Long item) {
        this.item = item;
    }

    public Long getVenda() {
        return venda;
    }

    public void setVenda(Long venda) {
        this.venda = venda;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ItemVendaPK that = (ItemVendaPK) o;

        if (item != null ? !item.equals(that.item) : that.item != null) return false;
        return venda != null ? venda.equals(that.venda) : that.venda == null;
    }

    @Override
    public int hashCode() {
        int result = item != null ? item.hashCode() : 0;
        result = 31 * result + (venda != null ? venda.hashCode() : 0);
        return result;
    }
}
