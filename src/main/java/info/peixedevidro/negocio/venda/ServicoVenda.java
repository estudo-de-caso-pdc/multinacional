package info.peixedevidro.negocio.venda;

import info.peixedevidro.entidade.ItemVenda;
import info.peixedevidro.entidade.Venda;
import info.peixedevidro.entidade.VendaMapper;
import info.peixedevidro.negocio.comum.RepositorioAbstrato;
import info.peixedevidro.negocio.dto.VendaDto;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

import static info.peixedevidro.entidade.QVenda.venda;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class ServicoVenda extends RepositorioAbstrato<Venda, Long> implements IServicoVenda {
    public List<VendaDto> listar() {
        return VendaMapper.MAPPER.toListVendaDto(super.selectFrom(venda).fetch());
    }

    public VendaDto obter(Long id) {
        return VendaMapper.MAPPER.toVendaDto(super.selectFrom(venda).where(venda.id.eq(id)).fetchOne());
    }

    @Transactional
    public void excluir(Long id) {
        remove(super.selectFrom(venda).where(venda.id.eq(id)).fetchOne());
    }

    @Transactional
    public VendaDto incluir(VendaDto vendaDto) {
        Venda venda = VendaMapper.MAPPER.toVenda(vendaDto);
        venda.setItemVendaList(new ArrayList<>());
        persistOrMerge(venda);
        for (ItemVenda itemVenda : VendaMapper.MAPPER.toVenda(vendaDto).getItemVendaList()) {
            itemVenda.setVenda(venda);
            venda.getItemVendaList().add(itemVenda);
        }

        return VendaMapper.MAPPER.toVendaDto(venda);
    }

    @Transactional
    public VendaDto atualizar(VendaDto vendaDto) {
        Venda entity = super.selectFrom(venda).where(venda.id.eq(vendaDto.getId())).fetchOne();
        VendaMapper.MAPPER.updateVenda(vendaDto, entity);

        return VendaMapper.MAPPER.toVendaDto(entity);
    }
}
