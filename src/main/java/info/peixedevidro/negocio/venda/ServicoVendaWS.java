package info.peixedevidro.negocio.venda;

import info.peixedevidro.negocio.dto.ItemDto;
import info.peixedevidro.negocio.dto.ItemVendaDto;
import info.peixedevidro.negocio.dto.SedeDto;
import info.peixedevidro.negocio.dto.VendaDto;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jws.WebService;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Stateless
@WebService
public class ServicoVendaWS {
    @EJB
    private IServicoVenda servicoVenda;

    public List<VendaDto> listar() {
        return servicoVenda.listar();
    }

    public VendaDto obter(Long id) {
        return servicoVenda.obter(id);
    }

    public void excluir(Long id) {
        servicoVenda.excluir(id);
    }

    public VendaDto incluir() {
        VendaDto vendaDto = new VendaDto();
        vendaDto.setDia(new Date());
        vendaDto.setSede(new SedeDto());
        vendaDto.getSede().setId(1L);

        ItemVendaDto itemVendaDto = new ItemVendaDto();
        itemVendaDto.setItem(new ItemDto());
        itemVendaDto.getItem().setId(15L);
        itemVendaDto.setQuantidade(5);

        vendaDto.setItemVendaList(Arrays.asList(itemVendaDto));

        return servicoVenda.incluir(vendaDto);
    }
}
