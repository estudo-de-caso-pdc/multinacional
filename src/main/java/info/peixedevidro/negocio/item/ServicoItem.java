package info.peixedevidro.negocio.item;

import info.peixedevidro.entidade.Item;
import info.peixedevidro.entidade.ItemMapper;
import info.peixedevidro.negocio.comum.RepositorioAbstrato;
import info.peixedevidro.negocio.dto.ItemDto;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.transaction.Transactional;
import java.util.List;

import static info.peixedevidro.entidade.QItem.item;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class ServicoItem extends RepositorioAbstrato<Item, Long> implements IServicoItem {
    @SuppressWarnings("unchecked")
    public List<ItemDto> listar() {
        return ItemMapper.MAPPER.toListItemDto(super.selectFrom(item).fetch());
    }

    public ItemDto obter(Long id) {
        return ItemMapper.MAPPER.toItemDto(super.selectFrom(item).where(item.id.eq(id)).fetchOne());
    }

    @Transactional
    public void excluir(Long id) {
        remove(super.selectFrom(item).where(item.id.eq(id)).fetchOne());
    }

    @Transactional
    public ItemDto incluir(ItemDto itemDto) {
        return ItemMapper.MAPPER.toItemDto(persistOrMerge(ItemMapper.MAPPER.toItem(itemDto)));
    }

    @Transactional
    public ItemDto atualizar(ItemDto atualizarItem) {
        Item entity;
        entity = super.selectFrom(item).where(item.id.eq(atualizarItem.getId())).fetchOne();
        ItemMapper.MAPPER.updateItem(atualizarItem, entity);

        return ItemMapper.MAPPER.toItemDto(entity);
    }
}
