package info.peixedevidro.negocio.comum;

import java.io.Serializable;

public interface Identificavel<T extends Serializable> extends Serializable {
    T getId();
}
