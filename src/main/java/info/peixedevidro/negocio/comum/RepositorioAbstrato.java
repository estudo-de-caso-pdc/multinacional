package info.peixedevidro.negocio.comum;

import com.querydsl.core.types.EntityPath;
import com.querydsl.core.types.Expression;
import com.querydsl.jpa.HQLTemplates;
import com.querydsl.jpa.impl.JPADeleteClause;
import com.querydsl.jpa.impl.JPAQuery;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.Serializable;

public abstract class RepositorioAbstrato <T extends Identificavel, PK extends Serializable> {

    @PersistenceContext
    private EntityManager em;

    protected <T> JPAQuery<T> selectFrom(EntityPath<T> entity) {
        return select(entity).from(entity);
    }

    protected <T> JPAQuery<T> select(Expression<T> select) {
        return new JPAQuery<>(em, HQLTemplates.DEFAULT).select(select);
    }

    protected JPADeleteClause delete(EntityPath<?> entity) {
        return new JPADeleteClause(em, entity, HQLTemplates.DEFAULT);
    }

    protected void detach(T entity) {
        em.detach(entity);
    }

    protected T find(Class<T> type, Long id) {
        return em.find(type, id);
    }

    protected void persist(T entity) {
        em.persist(entity);
    }

    protected T merge(T entity) {
        return em.merge(entity);
    }

    protected T persistOrMerge(T entity) {
        if (entity.getId() != null) {
            return merge(entity);
        }
        persist(entity);
        return entity;
    }

    protected void remove(T entity) {
        em.remove(entity);
    }
}
