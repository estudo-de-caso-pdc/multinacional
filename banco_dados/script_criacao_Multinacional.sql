ALTER TABLE `Multinacional`.`Venda` DROP FOREIGN KEY `fk_Venda_Sede`;
ALTER TABLE `Multinacional`.`ItemVenda` DROP FOREIGN KEY `fk_ItemVenda_Item`;
ALTER TABLE `Multinacional`.`ItemVenda` DROP FOREIGN KEY `fk_ItemVenda_Venda`;

DROP INDEX `pk_id` ON `Multinacional`.`Sede`;
DROP INDEX `pk_id` ON `Multinacional`.`Venda`;
DROP INDEX `fk_sede` ON `Multinacional`.`Venda`;
DROP INDEX `pk_item_venda` ON `Multinacional`.`ItemVenda`;
DROP INDEX `pk_id` ON `Multinacional`.`Item`;

DROP TABLE `Multinacional`.`Sede` CASCADE;
DROP TABLE `Multinacional`.`Venda` CASCADE;
DROP TABLE `Multinacional`.`ItemVenda` CASCADE;
DROP TABLE `Multinacional`.`Item` CASCADE;

CREATE TABLE `Multinacional`.`Sede` (
`id` int(11) NOT NULL,
`nome` varchar(255) NOT NULL,
PRIMARY KEY (`id`) ,
INDEX `pk_id` (`id` ASC)
);

CREATE TABLE `Multinacional`.`Venda` (
`id` int(11) NOT NULL AUTO_INCREMENT,
`sede` int(11) NULL,
`dia` timestamp NOT NULL,
PRIMARY KEY (`id`) ,
INDEX `pk_id` (`id` ASC),
INDEX `fk_sede` (`sede` ASC)
);

CREATE TABLE `Multinacional`.`ItemVenda` (
`item` int(11) NOT NULL,
`venda` int(11) NOT NULL,
`quantidade` int(11) NOT NULL,
PRIMARY KEY (`item`, `venda`) ,
INDEX `pk_item_venda` (`item` ASC, `venda` ASC)
);

CREATE TABLE `Multinacional`.`Item` (
`id` int(11) NOT NULL AUTO_INCREMENT,
`nome` varchar(255) NOT NULL,
`descricao` varchar(255) NULL,
`estoque` int(11) NOT NULL,
PRIMARY KEY (`id`) ,
INDEX `pk_id` (`id` ASC)
);


ALTER TABLE `Multinacional`.`Venda` ADD CONSTRAINT `fk_Venda_Sede` FOREIGN KEY (`sede`) REFERENCES `Multinacional`.`Sede` (`id`);
ALTER TABLE `Multinacional`.`ItemVenda` ADD CONSTRAINT `fk_ItemVenda_Item` FOREIGN KEY (`item`) REFERENCES `Multinacional`.`Item` (`id`);
ALTER TABLE `Multinacional`.`ItemVenda` ADD CONSTRAINT `fk_ItemVenda_Venda` FOREIGN KEY (`venda`) REFERENCES `Multinacional`.`Venda` (`id`);

